<?php 
namespace app\commands;

 // console command to add the rule
use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{

	public function actionOwnCategory()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnCategoryRule;
		$auth->add($rule);
	}
}
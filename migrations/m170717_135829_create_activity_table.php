<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `activity`.
 */
class m170717_135829_create_activity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('activity', [
            'id' => $this->primaryKey(),
            'title' => Schema::TYPE_STRING,
            'categoryId' => $this->integer(),
            'statusId' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activity');
    }
}

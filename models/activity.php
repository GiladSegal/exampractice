<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use app\models\Category;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryId
 * @property integer $statusId
 */
class activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
    //    return [
      //      [['categoryId', 'statusId'], 'integer'],
       //     [['title'], 'string', 'max' => 255],
       // ];
    

    $rules = []; 
        $stringItems = [['title'], 'string'];
        $integerItems  = ['statusId'];        
        if (\Yii::$app->user->can('updateActivity')) {//to prevent a category manager from changing the activities category
            $integerItems[] = 'categoryId';
        }
        $integerRule = [];
        $integerRule[] = $integerItems;
        $integerRule[] = 'integer';
        $rules[] = $stringItems;
        $rules[] = $integerRule;       
        return $rules;  
        }
    /**
     * @inheritdoc
     */
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }

public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }



    
}

<?php
namespace app\rbac;

use app\models\User;
use yii\rbac\Rule;
use Yii; 

class OwnCategoryRule extends Rule
{
	public $name = 'ownCategoryRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['activity']) ? $params['activity']->categoryId == (User::findIdentity($user)->CategoryId) : false;
			
		}
		return false;
	}
}
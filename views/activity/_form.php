<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Category;


/*
why not working??

<? if(\Yii::$app->user->can('updateActivity', ['user' => $model]) )  {
			echo $form->field($model, 'categoryId') -> dropDownList([disabled],Category::getCategories()); } ?> 
*/


/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    

	<?= $form->field($model,'categoryId')->textInput(['disabled' => !\Yii::$app->user->can('createNewActivity')]); ?>


		<?= $model->isNewRecord ? $form->field($model, 'statusId')->hiddenInput(['value'=> 2])->label(false) : $form->field($model, 'statusId')->
				dropDownList(Status::getStatuses()) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

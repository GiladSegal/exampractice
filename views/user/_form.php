<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\user;
use app\models\category;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <!--<?= $form->field($model, 'auth_Key')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $model->isNewRecord ? $form->field($model, 'role')->hiddenInput(['value'=> 'user'])->label(false) : $form->field($model, 'role')->dropDownList(User::getRoles()) ?>
    
    <?= $model->isNewRecord ? $form->field($model, 'CategoryId')->hiddenInput(['value'=> 'NULL'])->label(false) : $form->field($model, 'CategoryId')->dropDownList(Category::getCategories()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
